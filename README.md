# Mirth Plugins

This repository is a clone of https://github.com/nextgenhealthcare/connect, with the following plugins added:
* DICOM worklist

These mirth connect versions have a corresponding branch and are supported:
* 3.9.1
* 3.9.0
* 3.8.1
* 3.7.1

You can either:
* clone and build the project for one branch, reading the instructions in: server/README.txt or
* use the zip files in the master branch, which have been signed using a self-signed certificate

For any question, feel free to contact us at: info@smartwavesa.com
